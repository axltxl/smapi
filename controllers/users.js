/*
 * StoneMan API
 *
 * Copyright (c) Alejandro Ricoveri
 *
 * Usuarios
 */

 /**
  * Utilidades
  */
 var utils = require ('../../util'),
     db    = require ('../database');

 /**
 * GET /users/:user
 * Obtener información sobre un usuario en la
 * base de datos de ejemplo
 */
 exports.show = function (req, res, next) {
      if (users[req.params.user])
        res.send (users[req.params.user]);
      else return next ( new utils.Error.NotFound() ) ;
    };

/**
 * GET /users
 * Obtener usuarios de la base de datos
 */
 exports.index = function ( req, res, next ) {
       res.send(db.users);
    };

 /**
  * DELETE /users/:user
  * Borrar un usuario
  */
 exports.destroy = function ( req, res ) {
        delete users[req.params.user];
        res.send("OK!");
     };

 /**
  * POST /users
  * Crear usuario
  */
 exports.create = function ( req, res ){
        users[req.body.id] = {
            name: req.body.name,
            email: req.body.email};
        res.send('OK!');
    };

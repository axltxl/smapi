/*
 * StoneMan API
 *
 * Copyright (c) Alejandro Ricoveri
 *
 * Base de datos
 */

 /**
  * @see http://github.com/felixge/node-mysql
  */
 var mysql = require ('mysql');

 /**
  * Base de datos
  *
  * Representa una abstracción de obtención de conexión a base de datos.
  * Por ahora sólo otorga un pool de conexiones hacia
  * una base de datos MySQL
  *
  * @param options Opciones de conexión a base de datos (mysql)
  */
 var database = function (options) {
     // Pool de conexiones
     this.pool = mysql.createPool (options);
 };
 /**
  * Consulta a base de datos
  *
  * Realiza una consulta a base de datos de donde al haber una
  * respuesta se llamará a su respectivo callback con un indicador
  * de error y un arreglo quien lleva los resultados de la consulta.
  * Por ahora sólo funciona con MySQL.
  *
  * @param sql Consulta en SQL
  * @param callback Función que responde al resultado de la consulta
  */
 database.prototype.query = function (sql, callback)
 {
   /** @see http://github.com/felixge/node-mysql */
  this.pool.getConnection(function ( err, connection )
  {
    // De haber error, llamar al callback con el error
    if (err)
      callback (new Error(err.code), undefined);

    // Usar la conexión
    else {
      connection.query ( sql,
      function ( err, rows ) {
        // Concluir la conexión
        connection.end();

        // De haber error o una respuesta
        // nula, el callback poseerá un error
        // NotFound
        if ( err )
            err = new Error(err.code);

        // Finalmente llamar al callback
        callback ( err, rows );
      });
    }
  });
 };

 module.exports = database;

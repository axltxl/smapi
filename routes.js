/*
 * StoneMan API
 *
 * Copyright (c) Alejandro Ricoveri
 *
 * Rutas
 */

 var utils = require ('../util'),
     db    = require ('./database'),
     log   = utils.Log,
     auth = require ('./auth');

 // Exportaciones de módulo
 module.exports = {
   post  : [ auth.login],
   get   : [ auth.ping ],
   delete: [ auth.logout ]
 };

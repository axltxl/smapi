/*
 * StoneMan API
 *
 * Copyright (c) Alejandro Ricoveri
 *
 * Rutas/entradas del API REST de la aplicación
 */

 /**
  * Utilidades
  */
 var utils   = require ('../util');
 var log     = utils.Log;
 var auth = require ('./auth');


 exports = module.exports = {

    // Datos de la API
    name    : "stonecrm",
    version : "1.0",

    // Middleware
    middleware : [ auth.check ],

    /**
     * Rutas de la API de ejemplo
     */
    routes : require ('./routes')
 };

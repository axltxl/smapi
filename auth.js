/*
 * StoneMan API
 *
 * Copyright (c) Alejandro Ricoveri
 *
 * Autenticación y manejo de sesiones
 */

 var utils    = require ('../util'),
     crypto   = require ('crypto'),
     Database = require ('./database'),
     config   = require ('./config.json');
     
 
 // Base de datos
 var db = new Database ( config.database );

 // Raiz
 var path_root = '/auth' ;
 
 // Tabla de sesiones
 var sessions = [];

 //
 var loginDenied = function () {
     return new utils.Error.Unauthorized ("Login denegado");
 };

 //
 var sessionInvalid = function () {
   return new utils.Error.Unauthorized ("Sesión inválida");
 };

 /**
  * Chequeo de sesión
  * Verifica la cookie de sesión de parte del cliente
  */
 exports.check = function (req, res, next) {

   if ( req.url == path_root )
    return next();

   // ¿Existe la cookie?
   if  ( ! ("__scrm" in req.cookies) )
     return next ( sessionInvalid() );

   // session id
   sessid = req.cookies['__scrm'];

   // ¿Esa id de session estará registrada en base de datos?
   if ( ! (sessid in sessions) )
    return next ( sessionInvalid() );

   // It's all good buddy
   next();

 };

 /**
  * GET /auth
  *
  * Esta es una vulgar comprobación de la cookie de sesión
  * expresada en una entrada de la API, no es middleware.
  * Devuelve HTTP 200 en caso de que la session id sea válida
  * de lo contrario devolverá 403
  */
 exports.ping = {

   //
   path: path_root,

   //
   callback : function (req, res, next) {
      // ¿Existe la cookie?
      if  ( ! ("__scrm" in req.cookies) )
        return next ( sessionInvalid() );
      else {
        // session id
        sessid = req.cookies['__scrm'];

        // ¿Esa id de session estará registrada en base de datos?
        if ( ! (sessid in sessions) )
          return next ( sessionInvalid() );
        else res.send ( 200 );
      }
    }
 };

 /**
  * POST /auth
  *
  * Revisión de credenciales del usuario
  */
 exports.login = {

    // Path
    path: path_root,

    //
    callback: function (req, res, next)
    {
      // Verificación de existencia
      if (! ("username" in req.body && "password" in req.body) )
        return next ( loginDenied() );

      // Asignar credenciales
      var usr_name = req.body['username'];
      
      /* 
       * Es importante destacar que el algoritmo SHA-1 es el
       * usado para el hashing de passwords 
       */
      var usr_pass = crypto.createHash('sha1')
                            .update(req.body['password'])
                            .digest("hex");

      // Verificar nombre de usuario
      db.query ("SELECT * FROM usuarios where username='"+usr_name+"'", 
        function (err, rows) {
          //
          if ( err )
            return next ( loginDenied() );
          
          // De haber respuesta vacía, se 
          // deniega la petición
          if ( !rows.length )
            return next ( loginDenied() );
          
          /* Respuesta */
          var response = rows[0];

          // Verificación de contraseña
          if ( usr_pass !== response.password  )
            return next ( loginDenied() );

          // Generar sessionID
          // La sessionID es un token por el cual se
          // determina la autenticidad del usuario en
          // cada petición HTTP, esta sessionID estará
          // representada en una cookie del lado del cliente
          // y estará registrada en base de datos de parte
          // del servidor
          var sessionID =  crypto.createHash('sha256')
                            .update(Math.random().toString())
                            .digest("hex");

          // Guardar sessionID en base de datos para este usuario
          debugger;
          sessions[sessionID] = {username: usr_name, ip: req.ip};

          // Establecer la session cookie hacia el cliente
          res.cookie ( '__scrm', sessionID );

          // Enviar la respuesta
          res.send ({id : sessionID});
        }
      );
    }
 };

 /**
  * DELETE /session
  *
  * Logout
  */
 exports.logout = {

   // Path
   path : path_root,

  // Callback
   callback : function (req, res, next) 
   {
     if ('__scrm' in req.cookies) {
       // La eliminación de la ID de sesión
       // del usuario dependerá enteramente
       // de la existencia de la misma de parte
       // del cliente
       if ( req.cookies['__scrm'] in sessions ) {
         delete sessions[ req.cookies['__scrm'] ];
         res.send(200);
       }
       else 
         return next ( sessionInvalid() );
     }
     // No existe la cookie de sesión
     else
       return next ( sessionInvalid() );
   }
 };
